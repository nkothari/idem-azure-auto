NAME_PARAMETER = {
    "default": None,
    "doc": "The identifier for this state",
    "param_type": "str",
    "required": True,
    "target": "hardcoded",
    "target_type": "arg",
}

GET_REQUEST_FORMAT = r"""
    subscription_id = ctx.acct.subscription_id
    return {{ function.hardcoded.get_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200],
    )
"""

LIST_REQUEST_FORMAT = r"""
    ret = dict(comment="", result=True, ret={})
    subscription_id = ctx.acct.subscription_id

    async for page_result in hub.tool.azure.request.paginate(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200],
    ):
        resource_list = page_result.get("value", None)
        if resource_list:
            for resource in resource_list:
                ret["ret"][resource["id"]] = resource
    return ret
"""

CREATE_REQUEST_FORMAT = r"""
    subscription_id = ctx.acct.subscription_id

    # PUT operation to create a resource
    return {{ function.hardcoded.create_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200, 201],
        json=parameters,
    )
"""

UPDATE_REQUEST_FORMAT = r"""
    subscription_id = ctx.acct.subscription_id

    {% if function.hardcoded.patch_parameters %}
    response_get = {{ function.hardcoded.get_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200],
    )

    # PATCH operation to update a resource
    patch_parameters = {{ function.hardcoded.patch_parameters }}
    existing_resource = response_get["ret"]
    new_parameters = hub.tool.azure.request.patch_json_content(patch_parameters, existing_resource, parameters)

    if force_update:
        return {{ function.hardcoded.create_function }}(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200, 201],
            json=new_parameters,
        )
    else:
        return {{ function.hardcoded.patch_function }}(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200],
            json=new_parameters,
        )

    {% else %}
    ret = dict(
        comment="No update operation on {{ function.hardcoded.resource_name }} since Azure does not have PATCH api on {{ function.hardcoded.resource_name }}",
        result=True,
        ret=None
    )

    return ret
    {% endif %}
"""

DELETE_REQUEST_FORMAT = r"""
    subscription_id = ctx.acct.subscription_id

    return {{ function.hardcoded.delete_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200, 202, 204],
    )
"""

PRESENT_REQUEST_FORMAT = r"""
    result = dict(comment="", old_state=None, new_state=None, name=name, result=True)
    subscription_id = ctx.acct.subscription_id
    response_get = {{ function.hardcoded.get_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200],
    )

    if force_update:
        if ctx.get("test", False):
            result["comment"] = "Would force to update azure.{{ function.hardcoded.create_ref }}"
            return result
        response_force_put = {{ function.hardcoded.create_function }}(
                    ctx,
                    url=f"{{ function.hardcoded.path }}",
                    success_codes=[200, 201],
                    json=parameters,
        )
        if response_force_put["result"]:
            old_resource = response_get["ret"] if response_get["result"] else None
            result["old_state"] = old_resource
            result["new_state"] = response_force_put["ret"]
            result["comment"]=response_force_put["comment"],
            return result
        else:
            hub.log.debug(
                f"Could not force to update {{ function.hardcoded.resource_name }} {response_force_put['comment']} {response_force_put['ret']}"
            )
            result["result"] = False
            result["new_state"] = response_force_put["ret"]
            result["comment"]=response_force_put["comment"],
            return result

    if not response_get["result"]:
        if ctx.get("test", False):
            result["comment"]="Would create azure.{{ function.hardcoded.create_ref }}"
            return result

        if response_get["status"] == 404:
            # PUT operation to create a resource
            response_put = {{ function.hardcoded.create_function }}(
                ctx,
                url=f"{{ function.hardcoded.path }}",
                success_codes=[200, 201],
                json=parameters,
            )

            if not response_put["result"]:
                hub.log.debug(
                    f"Could not create {{ function.hardcoded.resource_name }} {response_put['comment']} {response_put['ret']}"
                )
                result["result"] = False

            result["comment"] = response_put["comment"] + " " + response_put["ret"]
        else:
            hub.log.debug(
                f"Could not get {{ function.hardcoded.resource_name }} {response_get['comment']} {response_get['ret']}"
            )
            result["new_state"] = response_get["ret"]
            result["comment"]=response_get["comment"],
        return result
    {% if function.hardcoded.patch_parameters %}
    else:
        # PATCH operation to update a resource
        patch_parameters = {{ function.hardcoded.patch_parameters }}
        existing_resource = response_get["ret"]
        new_parameters = hub.tool.azure.request.patch_json_content(patch_parameters, existing_resource, parameters)
        if ctx.get("test", False):
            result["comment"]=f"Would update azure.{{ function.hardcoded.create_ref }} with parameters: {new_parameters}",
            return result

        if not new_parameters:
            result["old_state"]=existing_resource
            result["new_state"]=existing_resource
            result["comment"]=f"'{name}' has no property need to be updated."
            return result

        response_patch = {{ function.hardcoded.patch_function }}(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200],
            json=new_parameters,
        )

        if not response_patch["result"]:
            hub.log.debug(
                f"Could not update {{ function.hardcoded.resource_name }} {response_patch['comment']} {response_patch['ret']}"
            )
            result["result"] = False
            result["comment"] = response_patch["comment"] + " " + response_patch["ret"]
            return result

        result["old_state"]=existing_resource
        result["new_state"]=response_patch["ret"]
        result["comment"]=response_patch["comment"]
        return result
    {% else %}
    # No update operation on {{ function.hardcoded.resource_name }} since Azure does not have PATCH api on {{ function.hardcoded.resource_name }}
    result["new_state"] = result["old_state"] = response_get["ret"]
    result["comment"]=response_get["comment"]
    return result
    {% endif %}
"""

ABSENT_REQUEST_FORMAT = r"""
    result = dict(comment="", old_state=None, new_state=None, name=name, result=True)
    subscription_id = ctx.acct.subscription_id
    response_get = {{ function.hardcoded.get_function }}(
        ctx,
        url=f"{{ function.hardcoded.path }}",
        success_codes=[200],
    )
    if response_get["result"]:
        if ctx.get("test", False):
            result["comment"] = "Would delete azure.{{ function.hardcoded.create_ref }}"
            return result

        existing_resource = response_get["ret"]
        response_delete = {{ function.hardcoded.delete_function }}(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200, 202, 204],
        )

        if not response_delete["result"]:
            hub.log.debug(
                f"Could not delete {{ function.hardcoded.resource_name }} {response_delete['comment']} {response_delete['ret']}"
            )
            result["result"] = False
            result["comment"] = response_delete["comment"] + " " + response_delete['ret']
            return result

        result["old_state"] = existing_resource
        result["new_state"] = {}
        result["comment"] = response_delete["comment"]
        return result
    elif response_get["status"] == 404:
        # If Azure returns 'Not Found' error, it means the resource has been absent.
        result["old_state"] = {}
        result["new_state"] = {}
        result["comment"] = f"'{name}' already absent"
        return result
    else:
        hub.log.debug(
            f"Could not get {{ function.hardcoded.resource_name }} {response_get['comment']} {response_get['ret']}"
        )
        result["result"] = False
        result["comment"] = response_get["comment"] + " " + response_get["ret"]
        return result
"""

DESCRIBE_REQUEST_FORMAT = r"""
    result = {}
    subscription_id = ctx.acct.subscription_id
    uri_parameters = {{ function.hardcoded.describe_parameters }}
    async for page_result in hub.tool.azure.request.paginate(
            ctx,
            url=f"{{ function.hardcoded.path }}",
            success_codes=[200],
    ):
        resource_list = page_result.get("value", None)
        if resource_list:
            for resource in resource_list:
                uri_parameter_values = (
                    hub.tool.azure.utils.get_uri_parameter_value_from_uri(resource["id"], uri_parameters)
                )
                result[resource["id"]] = {f"azure.{{ function.hardcoded.create_ref }}.present": uri_parameter_values
                + [{"parameters": resource}]}
    return result
"""
