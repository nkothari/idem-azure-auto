import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_virtual_network_full(hub, ctx, resource_group_fixture):
    """
    This test provisions a virtual network, describes virtual network, does a force update and deletes
     the provisioned virtual network.
    """
    # Create virtual network
    resource_group_name = resource_group_fixture.get("name")
    virtual_network_name = "idem-test-virtual-network-" + str(uuid.uuid4())
    vnet_parameters = {
        "location": "eastus",
        "address_space": ["10.0.0.0/26"],
        "flow_timeout_in_minutes": 10,
        "tags": {
            f"idem-test-tag-key-"
            + str(uuid.uuid4()): f"idem-test-tag-value-"
            + str(uuid.uuid4())
        },
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create virtual network with --test
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        test_ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        **vnet_parameters,
    )
    assert vnet_ret["result"], vnet_ret["comment"]
    assert not vnet_ret["old_state"] and vnet_ret["new_state"]
    assert (
        f"Would create azure.virtual_networks.virtual_networks '{virtual_network_name}'"
        in vnet_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=vnet_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vnet_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )
    resource_id = vnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
        == resource_id
    )

    # Create virtual network in real
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        **vnet_parameters,
    )
    assert vnet_ret["result"], vnet_ret["comment"]
    assert not vnet_ret["old_state"] and vnet_ret["new_state"]
    check_returned_states(
        old_state=None,
        new_state=vnet_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vnet_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )
    resource_id = vnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Describe virtual network
    describe_ret = await hub.states.azure.virtual_networks.virtual_networks.describe(
        ctx
    )
    assert resource_id in describe_ret
    describe_resource = describe_ret.get(resource_id)
    described_resource = describe_resource.get(
        "azure.virtual_networks.virtual_networks.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        virtual_network_name=virtual_network_name,
        resource_group_name=resource_group_name,
        idem_resource_name=resource_id,
    )

    vnet_update_parameters = {
        "location": "eastus",
        "address_space": ["10.0.0.0/27"],
        "flow_timeout_in_minutes": 15,
        "tags": {
            f"idem-test-tag-key-"
            + str(uuid.uuid4()): f"idem-test-tag-value-"
            + str(uuid.uuid4())
        },
    }
    # Update virtual network with --test
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        test_ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        **vnet_update_parameters,
    )
    assert vnet_ret["result"], vnet_ret["comment"]
    assert vnet_ret["old_state"] and vnet_ret["new_state"]
    assert (
        f"Would update azure.virtual_networks.virtual_networks '{virtual_network_name}'"
        in vnet_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_ret["old_state"],
        new_state=vnet_ret["new_state"],
        expected_old_state=vnet_parameters,
        expected_new_state=vnet_update_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )
    resource_id = vnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
        == resource_id
    )

    # Update virtual network in real
    vnet_ret = await hub.states.azure.virtual_networks.virtual_networks.present(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        **vnet_update_parameters,
    )
    assert vnet_ret["result"], vnet_ret["comment"]
    assert vnet_ret["old_state"] and vnet_ret["new_state"]
    assert (
        f"Updated azure.virtual_networks.virtual_networks '{virtual_network_name}'"
        in vnet_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_ret["old_state"],
        new_state=vnet_ret["new_state"],
        expected_old_state=vnet_parameters,
        expected_new_state=vnet_update_parameters,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )
    resource_id = vnet_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}/providers/Microsoft.Network/virtualNetworks/{virtual_network_name}"
        == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete virtual network with --test
    vnet_del_ret = await hub.states.azure.virtual_networks.virtual_networks.absent(
        test_ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"Would delete azure.virtual_networks.virtual_networks '{virtual_network_name}'"
        in vnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=vnet_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )

    # Delete virtual network in real
    vnet_del_ret = await hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"Deleted azure.virtual_networks.virtual_networks '{virtual_network_name}'"
        in vnet_del_ret["comment"]
    )
    check_returned_states(
        old_state=vnet_del_ret["old_state"],
        new_state=None,
        expected_old_state=vnet_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
        idem_resource_name=virtual_network_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-03-01",
        retry_count=10,
        retry_period=10,
    )

    # Delete virtual network again
    vnet_del_ret = await hub.states.azure.virtual_networks.virtual_networks.absent(
        ctx,
        name=virtual_network_name,
        resource_group_name=resource_group_name,
        virtual_network_name=virtual_network_name,
    )
    assert vnet_del_ret["result"], vnet_del_ret["comment"]
    assert not vnet_del_ret["old_state"] and not vnet_del_ret["new_state"]
    assert (
        f"azure.virtual_networks.virtual_networks '{virtual_network_name}' already absent"
        in vnet_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    virtual_network_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert virtual_network_name == old_state.get("virtual_network_name")
        assert expected_old_state["address_space"] == old_state.get("address_space")
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["flow_timeout_in_minutes"] == old_state.get(
            "flow_timeout_in_minutes"
        )
        assert expected_old_state["tags"] == old_state.get("tags")
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert virtual_network_name == new_state.get("virtual_network_name")
        assert expected_new_state["address_space"] == new_state.get("address_space")
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["flow_timeout_in_minutes"] == new_state.get(
            "flow_timeout_in_minutes"
        )
        assert expected_new_state["tags"] == new_state.get("tags")
